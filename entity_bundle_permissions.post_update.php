<?php

/**
 * @file
 * Post-update routines for this module.
 *
 * Copyright (C) 2023  Library Solutions, LLC (et al.).
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 */

/**
 * Trigger a cache rebuild for changes in #3409805.
 */
function entity_bundle_permissions_post_update_cache_rebuild_3409805() {
}
